# Модели
class PromoMessage < ActiveRecord::Base
  validates :body,      presence: true
  validates :date_from, presence: true
  validates :date_to,   presence: true
end

class User < ActiveRecord::Base
  has_many :ads

  scope :recent, -> { order(id: :desc) }

  scope :with_one_published, ->(from, to) do
    joins(:ads).group("ads.user_id")
      .where("`published_ads_count` = 1")
      .where("published_at Between ? AND ?", from, to)
  end
end

class Ad < ActiveRecord::Base
  belongs_to :user
end


# Контроллеры
class PromoMessagesController < ApplicationController
  attr_reader :provider

  def new
    @message = PromoMessage.new
    @users   = get_users.page(params[:page])
  end

  def create
    @message = PromoMessage.new(promo_message_params)

    if @message.save
      AdRecipientsService.new(get_users).send_message

      redirect_to promo_messages_path, notice: "Messages Sent Successfully!"
    else
      render :new
    end
  end

  def download_csv
    service = AdRecipientsService.new(get_users)
    file_name = "promotion-users-#{Time.zone.today}.csv"

    send_data service.generate_file, filename: filename
  end

  private

    def get_users
      from, to = params[:date_from].to_date, params[:date_to].to_date
      return [] unless from && to

      User.recent.with_one_published(from, to)
    end

    def promo_message_params
      params.permit(:body, :date_from, :date_to)
    end
end


# Сервисы
class AdRecipientsService

  def initialize(users)
    @users ||= users
  end

  def send_message
    @users.each do |user|
      PromoMessagesSendJob.perform_later(user.phone)
    end
  end

  def generate_file
    attributes = %w(id phone name)
    rows = @users.pluck(attributes)

    CSV.generate(headers: true) do |csv|
      csv << attributes

      rows.each { |row| csv << row }
    end
  end
end
